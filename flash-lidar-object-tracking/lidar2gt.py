import os
import numpy as np
import glob
import pickle
import xml.etree.ElementTree as ET

def main():


    new_annotations = []
    xml_path = 'recording_2019-02-13 13:13:24/recording_2019-02-13 13:13:24.xml'
    tree = ET.parse(xml_path)  
    root = tree.getroot()
    
    width = float(root.find('meta').find('task').find('original_size').find('width').text)
    height = float(root.find('meta').find('task').find('original_size').find('height').text)
    print(width, height)
    count = 0
    for track in root.iter('track'):  
        label = track.attrib['label']
        for box in track:
            new_gt = np.zeros(10)
            
            # extract xtl, ytl, xbr, ybr
            xtl = float(box.get('xtl'))
            ytl = float(box.get('ytl'))
            xbr = float(box.get('xbr'))
            ybr = float(box.get('ybr'))


            w = (xbr - xtl)
            h = (ybr - ytl)
            
            
            labelCategory = None
            for Category in box:
                labelCategory = Category.text
            if labelCategory is not None:
                labelClass = label + ' ' + labelCategory
            else:
                labelClass = label

            frame = box.get('frame')
            new_gt[0] = int(frame) + 1
            new_gt[1] = int(track.get('id')) + 1
            new_gt[2] = xtl
            new_gt[3] = ytl
            new_gt[4] = w
            new_gt[5] = h
            new_gt[6] = 1
            new_gt[7] = -1
            new_gt[8] = -1
            new_gt[9] = -1
            count += 1
            print(count)
            new_annotations.append(new_gt)


    new_annotations = np.vstack(new_annotations)
    print(new_annotations.shape)
    fmt = '%d,%d,%.3f,%.3f,%.3f,%.3f,%d,%d,%d,%d'
    np.savetxt('fixed_annotation.txt',new_annotations, fmt=fmt)   
            #newLabelFilename = labels_train_directory + '/'+ directory_name+ '_train_' + 'lidar-' + frame + '.txt'
            #labelLine = str(category_dict[labelClass]) + ' ' + str(x) + ' ' + str(y) + ' ' + str(w) + ' ' + str(h)
            #with open(newLabelFilename, 'a+') as label_file:
                 #label_file.write(labelLine+'\n')
            


    '''
    annotationFiles = glob.glob("/2018-12-11_scene-3_4200-4800/*.txt")

    final_detections = []
    for ii in range(600):

        gt = np.genfromtxt('2018-12-11_scene-3_4200-4800/2018-12-11_scene-3_4200-4800_val_lidar-{}.mat.txt'.format(ii))


            
    final_detections = np.vstack(final_detections)
    print(final_detections)
    print(final_detections.shape)
    print(final_detections[final_detections[:,0] == 0])
    
    fmt = '%d,%d,%.3f,%.3f,%.3f,%.3f,%.3f,%d,%d,%d'
    np.savetxt('lidar-4200-4800-scene3.txt',final_detections, fmt=fmt)
    '''

if __name__ == "__main__":
    main()
