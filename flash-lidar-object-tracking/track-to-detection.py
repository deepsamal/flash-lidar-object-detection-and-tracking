from __future__ import division
import numpy as np
import os
import xml.etree.ElementTree as ET
import scipy.io
import glob

track_filename = 'recording_2019-02-13 13:13:24/recording_2019-02-13 13:13:24.xml'
input_folder = 'recording_2019-02-13 13:13:24'

tree = ET.parse(track_filename)  
root = tree.getroot()

meta = root.find('meta')
task = meta.find('task')
original_size = task.find('original_size')
width = original_size.find('width')
height = original_size.find('height')

def get_sorted_file_list(input_folder):
    file_list = glob.glob(input_folder + '/*.mat')
    sorted_file_list = sorted(file_list, key=lambda x: int((x.split('.')[0]).split('-')[-1]))
    return sorted_file_list

def read_lidar_frame(lidarFilename):
    depth_image = None
    if os.path.exists(lidarFilename):
        lidar_image = scipy.io.loadmat(lidarFilename)['lidar_data']
        depth_image = lidar_image[0]
    else:
        print('Could not find file: ' + lidarFilename)
    
    return depth_image

def get_box_depth(depth_image, xtl, ytl, xbr, ybr):
    xtl = int(xtl)
    ytl = int(ytl)
    xbr = int(xbr)
    ybr = int(ybr)
    #print(xtl, ytl, xbr, ybr, (xtl+xbr)/2, (ytl+ybr)/2)
    center_depth = depth_image[int((ytl+ybr)/2), int((xtl+xbr)/2)]
    #median_depth = np.median(depth_image[ytl:ybr, xtl:xbr])
    return center_depth

def append_file(filename, detection):
    f=open(filename, "a+")
    f.write(detection+'\n')

file_list = get_sorted_file_list(input_folder)

# find object detection/gt position
for track in root.iter('track'):  
    for box in track:
        frame_number = int(box.get('frame'))
        xtl = float(box.get('xtl'))
        ytl = float(box.get('ytl'))
        xbr = float(box.get('xbr'))
        ybr = float(box.get('ybr'))
        outside = int(box.get('outside'))
        if outside == 1:
            continue

        #print(frame, xtl, ytl, xbr, ybr)

        # find mean depth of the box
        depth_image = read_lidar_frame(file_list[frame_number])
        if depth_image is not None:
            box_depth = get_box_depth(depth_image, xtl, ytl, xbr, ybr)
            detection = "{:d} {:f} {:f} {:f} {:f} {:f}".format(frame_number, xtl, ytl, (xbr-xtl), (ybr-ytl), box_depth)
            append_file(input_folder+'.txt', detection)
