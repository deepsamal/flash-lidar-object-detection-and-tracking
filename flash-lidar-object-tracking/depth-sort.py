"""
    SORT: A Simple, Online and Realtime Tracker
    Copyright (C) 2016 Alex Bewley alex@dynamicdetection.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from __future__ import print_function

from numba import jit
import os.path
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from skimage import io
from sklearn.utils.linear_assignment_ import linear_assignment
import glob
import time
import argparse
from filterpy.kalman import KalmanFilter
import cv2
import scipy.io
import time
@jit



def iou(bb_test,bb_gt):
    """
    Computes IOU - INTERSECTION OVER UNION between two bboxes in the form [x1,y1,x2,y2]
    """
    xx1 = np.maximum(bb_test[0], bb_gt[0])
    yy1 = np.maximum(bb_test[1], bb_gt[1])
    xx2 = np.minimum(bb_test[2], bb_gt[2])
    yy2 = np.minimum(bb_test[3], bb_gt[3])

    width = np.maximum(0., xx2 - xx1)
    height = np.maximum(0., yy2 - yy1)

    area = width * height

    overlap = area / ((bb_test[2]-bb_test[0])*(bb_test[3]-bb_test[1])
    + (bb_gt[2]-bb_gt[0])*(bb_gt[3]-bb_gt[1]) - area)

    #print(overlap)

    return(overlap)


def depthDistance(detectionDepth, predictedDepth):
    '''
    Take the detected depth and predicted depth, returns the euclidean distance between
    a detection and a given track for use in linear assignment
    '''

    print(detectionDepth)
    print(predictedDepth)
    depthDistance = np.sqrt((detectionDepth[4] - predictedDepth[4])**2)

    return depthDistance
    
def cornersToScaleBox(bbox):
    """
    Takes a bounding box in the form [x1,y1,x2,y2] and returns z in the form
    [x,y,s,r] where x,y is the centre of the box and s is the scale/area and r is
    the aspect ratio
    """
    width = bbox[2] - bbox[0]
    height = bbox[3] - bbox[1]
    centerX = bbox[0] + width/2.
    centerY = bbox[1] + height/2.

    area = width*height    #scale is just area

    aspectRatio = width/float(height)

    return np.array([centerX,centerY,area,aspectRatio]).reshape((4,1))


def scaleToCornersBox(x,score=None):
    """
    Takes a bounding box in the centre form [x,y,s,r] and returns it in the form
    [x1,y1,x2,y2] where x1,y1 is the top left and x2,y2 is the bottom right
    """
    #print("This is X[0]")
    #print(x[0][0])
    w = np.sqrt(x[3][0]*x[4][0])
    h = x[3][0]/w
    if(score==None):
        return np.array([x[0][0]-w/2.,x[1][0]-h/2.,x[0][0]+w/2.,x[1][0]+h/2., x[2][0]]).reshape((1,5))
    else:
        return np.array([x[0][0]-w/2.,x[1][0]-h/2.,x[0][0]+w/2.,x[1][0]+h/2.,score]).reshape((1,5))



def BoxToState(bbox):

    x, y, area, ratio = cornersToScaleBox(bbox[:4])
    z = bbox[-1]

    return np.array([x, y, z, area, ratio]).reshape(5,1)

def get_box_depth(depth_image, xtl, ytl, xbr, ybr):
    xtl = int(xtl)
    ytl = int(ytl)
    xbr = int(xbr)
    ybr = int(ybr)
    x_center = int((xtl+xbr)/2)
    y_center = int((ytl+ybr)/2)
    x_max = depth_image.shape[1] - 1
    y_max = depth_image.shape[0] - 1
    if x_center > x_max or x_center < 0 or y_center > y_max or y_center < 0:
        return -1
    center_depth = depth_image[y_center, x_center]
    #median_depth = np.median(depth_image[ytl:ybr, xtl:xbr])
    return center_depth


class KalmanBoxTracker(object):
    """
    This class represents the internel state of individual tracked objects observed as bbox.
    """
    count = 0
    def __init__(self,bbox):
        """
        Initialises a tracker using initial bounding box.
        """
        #define constant velocity model
        self.kf = KalmanFilter(dim_x=9, dim_z=5)
        self.kf.F = np.array([[1,0,0,0,0,1,0,0,0],
                              [0,1,0,0,0,0,1,0,0],
                              [0,0,1,0,0,0,0,1,0],
                              [0,0,0,1,0,0,0,0,1],
                              [0,0,0,0,1,0,0,0,0],  
                              [0,0,0,0,0,1,0,0,0],
                              [0,0,0,0,0,0,1,0,0],
                              [0,0,0,0,0,0,0,1,0],
                              [0,0,0,0,0,0,0,0,1]])

        self.kf.H = np.array([[1,0,0,0,0,0,0,0,0],
                              [0,1,0,0,0,0,0,0,0],
                              [0,0,1,0,0,0,0,0,0],
                              [0,0,0,1,0,0,0,0,0],
                              [0,0,0,0,1,0,0,0,0]])

        self.kf.R[3:,3:] *= 10.
        self.kf.P[5:,5:] *= 1000. #give high uncertainty to the unobservable initial velocities
        self.kf.P *= 10.
        self.kf.Q[-1,-1] *= 0.01
        self.kf.Q[5:,5:] *= 0.01

        #TODO Update this measurement to include lidar
        
        self.kf.x[:5] = BoxToState(bbox)
        self.time_since_update = 0
        self.id = KalmanBoxTracker.count
        KalmanBoxTracker.count += 1
        self.history = []
        self.hits = 0
        self.hit_streak = 0
        self.age = 0

    def update(self,bbox):
        """
        Updates the state vector with observed bbox.
        """
        self.time_since_update = 0
        self.history = []
        self.hits += 1
        self.hit_streak += 1
        self.kf.update(BoxToState(bbox))

    def predict(self):
        """
        Advances the state vector and returns the predicted bounding box estimate.
        """
        if((self.kf.x[8]+self.kf.x[3])<=0):
            self.kf.x[8] *= 0.0
        self.kf.predict()
        self.age += 1
        if(self.time_since_update>0):
            self.hit_streak = 0
        self.time_since_update += 1
        self.history.append(scaleToCornersBox(self.kf.x))
        return self.history[-1]

    def get_state(self):
        """
        Returns the current bounding box estimate.
        """
        return scaleToCornersBox(self.kf.x)

def associateDetectionsToTrackers(detections,trackers,iou_threshold = 0.3):
    """
    Assigns detections to tracked object (both represented as bounding boxes)

    Returns 3 lists of matches, unmatched_detections and unmatched_trackers


    TODO: FIX THIS UGLY CODE
    """
    if(np.any(np.isnan(trackers))):
        return np.empty((0,2),dtype=int), np.arange(len(detections)), np.empty((0,6),dtype=int)

    if(len(trackers)==0):
        return np.empty((0,2),dtype=int), np.arange(len(detections)), np.empty((0,6),dtype=int)

    iou_matrix = np.zeros((len(detections),len(trackers)),dtype=np.float32)
    depthDistanceMatrix = np.zeros((len(detections),len(trackers)),dtype=np.float32)
    costMatrix = np.zeros((len(detections),len(trackers)),dtype=np.float32)

    
    for d,det in enumerate(detections):
        for t,trk in enumerate(trackers):
            iou_matrix[d,t] = iou(det,trk)
            depthDistanceMatrix[d,t] = depthDistance(det,trk)

    

    print('This is the iou matrix')
    print(-iou_matrix)

    print('This is the depthDistanceMatrix')
    print(depthDistanceMatrix/100)
    print('')

    depthDistanceMatrix[np.isnan(depthDistanceMatrix)] = 15

    costMatrix = depthDistanceMatrix/400 + (-iou_matrix)

    

    matched_indices = linear_assignment(costMatrix)

    unmatched_detections = []
    for d,det in enumerate(detections):
        if(d not in matched_indices[:,0]):
            unmatched_detections.append(d)
    unmatched_trackers = []
    for t,trk in enumerate(trackers):
        if(t not in matched_indices[:,1]):
            unmatched_trackers.append(t)

  #filter out matched with low IOU
    matches = []
    for m in matched_indices:
        if(iou_matrix[m[0],m[1]]<iou_threshold):
            unmatched_detections.append(m[0])
            unmatched_trackers.append(m[1])
        else:
            matches.append(m.reshape(1,2))
    if(len(matches)==0):
        matches = np.empty((0,2),dtype=int)
    else:
        matches = np.concatenate(matches,axis=0)

    return matches, np.array(unmatched_detections), np.array(unmatched_trackers)


class Sort(object):
    def __init__(self,maxTrackAge=1,minHits=3):
        """
        Params:
            maxTrackAge - the maximum number of frames a track can go without an associated detection
                          to still be considered a valid
            minHits     - The minimum number of associated detections a track need to be considered as
                          a valid track

        Sets key parameters for SORT
        """
        self.maxTrackAge = maxTrackAge
        self.minHits = minHits
        self.existingTrackers = []
        self.frameCount = 0

    def update(self,detections, rawDepthImage):
        """
        Params:
          dets - a numpy array of detections in the format [[x1,y1,x2,y2,score],[x1,y1,x2,y2,score],...]
        Requires: this method must be called once for each frame even with empty detections.
        Returns the a similar array, where the last column is the object ID.

        NOTE: The number of objects returned may differ from the number of detections provided.
        """
        print(detections.shape)
        self.frameCount += 1

        print("The Current Frame Count is {}".format(self.frameCount))

        #get predicted locations from existing trackers.
        activeTracks = np.zeros((len(self.existingTrackers),6))
        trackersToDelete = []
        validTracks = []

        for trackIndex, track in enumerate(activeTracks):

            pos = self.existingTrackers[trackIndex].predict()[0]
            track[:] = [pos[0], pos[1], pos[2], pos[3],pos[4], 0]
            if(np.any(np.isnan(pos))):
                trackersToDelete.append(trackIndex)
        activeTracks = np.ma.compress_rows(np.ma.masked_invalid(activeTracks))
       
        #Delete trackers that make invalid predictions
        for trackIndex in reversed(trackersToDelete):
            self.existingTrackers.pop(trackIndex)

        #Associate detections to the trackers
        matchedIndices, unmatchedDetectionIndices, unmatchedTrackIndices = associateDetectionsToTrackers(detections,activeTracks)

        #update matched trackers with assigned detections
        for trackIndex, track in enumerate(self.existingTrackers):
            if(trackIndex not in unmatchedTrackIndices):
                d = matchedIndices[np.where(matchedIndices[:,1]==trackIndex)[0],0]
                track.update(detections[d,:][0])
                
                '''
                print("This is F")
                print(track.kf.F)
                print(track.kf.F.shape)
                print("This is H")
                print(track.kf.H) 
                print(track.kf.H.shape)
                print("This is P")
                print(track.kf.P) 
                print(track.kf.P.shape)
                print("This is Q")
                print(track.kf.Q) 
                print(track.kf.Q.shape)
                print("This is R") 
                print(track.kf.R) 
                print(track.kf.R.shape)
                print("This is x")
                print(track.kf.x) 
                print(track.kf.x.shape)
                print(track.kf.K) 
                print(track.kf.K.shape)
                print(track.kf.y) 
                print(track.kf.y.shape)
                '''
        np.set_printoptions(formatter={'float': lambda x: "{0:0.5f}".format(x)})
        #create and initialise new trackers for unmatched detections
        for unmatchedIndex in unmatchedDetectionIndices:
            newTrack = KalmanBoxTracker(detections[unmatchedIndex,:])
            '''
            print("This is F")
            print(newTrack.kf.F)
            print(newTrack.kf.F.shape)
            print("This is H")
            print(newTrack.kf.H) 
            print(newTrack.kf.H.shape)
            print("This is P")
            print(newTrack.kf.P) 
            print(newTrack.kf.P.shape)
            print("This is Q")
            print(newTrack.kf.Q) 
            print(newTrack.kf.Q.shape)
            print("This is R") 
            print(newTrack.kf.R) 
            print(newTrack.kf.R.shape)
            print("This is x")
            print(newTrack.kf.x) 
            print(newTrack.kf.x.shape)
            print(newTrack.kf.K) 
            print(newTrack.kf.K.shape)
            print(newTrack.kf.y) 
            print(newTrack.kf.y.shape)
            '''
            self.existingTrackers.append(newTrack)


        trackIndex = len(self.existingTrackers)
        
        for track in reversed(self.existingTrackers):
            
            d = track.get_state()[0]

            if((track.time_since_update < 1) and (track.hit_streak >= self.minHits or self.frameCount <= self.minHits)):
                validTracks.append(np.concatenate((d,[track.id+1])).reshape(1,-1)) # +1 as MOT benchmark requires positive

            trackIndex -= 1
            occlusion_aware = False

            if occlusion_aware:
                # do not remove if occluded
                # xtl, ytl = d[0], d[1] ... xbr, ybr = d[2], d[3]
                # get current depth at position
                depth = get_box_depth(rawDepthImage, d[0], d[1], d[2], d[3])
                #print(depth, d[4])    # actual depth @ location, predicted depth of tracker
                if (d[4] - depth < 1) or depth == -1:    # if tracklet predicted distance is almost same as actual distance, then target is probably not occluded, so remove it according to principle
                    #remove dead tracklet
                    if(track.time_since_update > self.maxTrackAge):
                        self.existingTrackers.pop(trackIndex)
            else:
                #remove dead tracklet
                if(track.time_since_update > self.maxTrackAge):
                    self.existingTrackers.pop(trackIndex)


        if(len(validTracks)>0):
            return np.concatenate(validTracks)

        return np.empty((0,6))


def parseCamelDetections(fullDetections, currentFrame):
    '''
    
    params: 
        fullDetections - Full array of detections
        currentFrame - the current video frame being processed
    return:
        detections - The detections from the current video frame
 
    '''
    detections = fullDetections[fullDetections[:,0]==currentFrame,1:]

    detections[:,2:4] += detections[:,0:2] #convert to [x1,y1,w,h,z] to [x1,y1,x2,y2,z]

    return detections


def getDepthValue():

    return depth





def parseSequenceNumber(sequenceName):
    '''
    params: 
        sequenceName - Full sequence string
    Return: SeqX, where X is the sequence number
    '''
    sequenceNumber, _ = sequenceName.split('-')

    return sequenceNumber

def parseSequenceModality(sequenceName):
    '''
    params:
        sequenceName - Full sequence string
    Return:
        sequenceModality - The modality of the video sequence visual\IR
    '''
    _, modalityString = sequenceName.split('-')

    sequenceModality, _ = modalityString.split('_')

    if sequenceModality == 'Vis':
        sequenceModality = 'Visual'

    return sequenceModality

def camelDatasetSequences():
    '''
    Params: 
        None - Takes no parameters just creates a list of CAMEL dataset video sequence names
    Return:
        sequences - list of CAMEL dataset sequence names
    '''

    sequences = []
    for sequenceIndex in range(1,31):
        if sequenceIndex in (12, 14, 16,24):
            continue
        for mode in ['IR','Vis']:
            seq = 'Seq{}-{}_det'.format(sequenceIndex,mode)
            sequences.append(seq)   

    return sequences

def integerColors(trackID, colors):
    '''
    Params:
        trackID - the track ID used, to create a unique color
        colors  - random list of colors to choose from, float between 0 - 1
    Return:
        colors - a unique color for a track as a unsigned 8 bit integer so it can be used with
                 opencv functions
    '''


    colors = colors[trackID%32,:]*255
    colors = colors.astype(np.uint8)

    return colors

def parse_args():
    """Parse input arguments."""
    parser = argparse.ArgumentParser(description='SORT demo')
    parser.add_argument('--display', dest='display', help='Display online tracker output (slow) [False]',action='store_true')
    args = parser.parse_args()
    return args


if __name__ == '__main__':
    # all train

    #'recording_2019-02-15_16:11:03_det_depth.txt'
    sequences = ['recording_2019-02-13 13:13:24']

    args = parse_args()
    display = args.display
    writeToVideo = True
    phase = 'train'
    totalTime = 0.0
    totalFrames = 0
    colors = np.random.rand(32,3) #used only for display


    if(display):
        plt.ion()
        fig = plt.figure() 
  
    if not os.path.exists('output'):
        os.makedirs('output')

    if (writeToVideo):
        fourcc = cv2.VideoWriter_fourcc(*'MP4V')

    for currentSequence in sequences:

        mot_tracker = Sort() #create instance of the SORT tracker

        print(currentSequence)

        if (writeToVideo):
            vidcap = cv2.VideoWriter('{}-tracks.mp4'.format(currentSequence),fourcc, 10.0, (1280,1024))

        fullDetections = np.genfromtxt('{}.txt'.format(currentSequence)) #load detections'

        with open('output/{}.txt'.format(currentSequence),'w') as out_file:

            print("Processing {}.".format(currentSequence))

            for currentFrame in range(int(fullDetections[:,0].max())):

                #currentFrame += 1 #detection and frame numbers begin at 1
                detections = parseCamelDetections(fullDetections, currentFrame)
               
                totalFrames += 1
   
                #sequenceNumber = parseSequenceNumber(currentSequence)
                #sequenceModality = parseSequenceModality(currentSequence)

                lidarFilename = '/home/cpslab/sort/recording_2019-02-13 13:13:24/lidar-{:d}.mat'.format(currentFrame)
                if os.path.exists(lidarFilename):
                    lidar_image = scipy.io.loadmat(lidarFilename)['lidar_data']
                    depth_image = (lidar_image[0]/400 * 255).astype(np.uint8)
                    if(display):
                        ax1 = fig.add_subplot(111, aspect='equal')
                        [p.remove() for p in reversed(ax1.patches)]
                        for txt in ax1.texts:
                            txt.set_visible(False)
                        im =depth_image
                        ax1.imshow(im)
                        plt.title(currentSequence+' Tracked Targets')
                else:
                    print('Lidar file not found:' + lidarFilename)

                startTime = time.time()
                trackers = mot_tracker.update(detections, lidar_image[0])
                cycleTime = time.time() - startTime
                totalTime += cycleTime
                display_image = cv2.cvtColor(depth_image, cv2.COLOR_GRAY2RGB)

                for d in trackers:
                    print('{:d},{:d},{:.2f},{:.2f},{:.2f},{:.2f},1,-1,-1,-1'.format(currentFrame,int(d[5]),d[0],d[1],d[2]-d[0],d[3]-d[1]),file=out_file)
                    #print(d)
                    d = d.astype(np.int32)
                    intColors = integerColors(d[5],colors)
                    imageWithTrack = cv2.rectangle(display_image,(d[0],d[1]),(d[2],d[3]),(int(intColors[0]),int(intColors[1]),int(intColors[2])),2)
                    
                    if(display):
                        d = d.astype(np.int32)
                        ax1.add_patch(patches.Rectangle((d[0],d[1]),d[2]-d[0],d[3]-d[1],fill=False,lw=3,ec=colors[d[5]%32,:]))
                        ax1.text(d[0],d[1], str(d[5]), fontsize=12)
                        ax1.set_adjustable('box-forced')


                if(writeToVideo):
                    if len(trackers) == 0:
                        vidcap.write(display_image)
                    else:
                        vidcap.write(imageWithTrack)

                if(display):
                    fig.canvas.flush_events()
                    plt.draw()
                time.sleep(1/2)

    if(writeToVideo):
        vidcap.release()

    print("Total Tracking took: {:.3f} for {:d} frames or {:.1f} FPS".format(totalTime,totalFrames,totalFrames/totalTime))
    if(display):
        print("Note: to get real runtime results run without the option: --display")



