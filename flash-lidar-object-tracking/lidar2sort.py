import os
import numpy as np
import glob
import pickle


def main():


    final_detections = []
    for ii in range(600):
        
        try:
            detection = np.genfromtxt('recording_2019-02-13 13:13:24/recording_2019-02-13 13:13:24_val_lidar-{}.mat.txt'.format(ii))
        except:
            continue

        if (detection.ndim == 1):
            new_detection = np.zeros((10))
            new_detection[0] = ii+1
            new_detection[1] = -1
            new_detection[2] = detection[0]
            new_detection[3] = detection[1]
            new_detection[4] = detection[2]-detection[0]
            new_detection[5] = detection[3]-detection[1]
            new_detection[6] = detection[5]
            new_detection[7] = -1
            new_detection[8] = -1
            new_detection[9] = -1
            final_detections.append(new_detection)
        elif (detection.ndim == 2):
            new_detection = np.zeros((detection.shape[0],10))
            new_detection[:,0] = ii+1
            new_detection[:,1] = -1
            new_detection[:,2] = detection[:,0]
            new_detection[:,3] = detection[:,1]
            new_detection[:,4] = detection[:,2]-detection[:,0]
            new_detection[:,5] = detection[:,3]-detection[:,1]
            new_detection[:,6] = detection[:,5]
            new_detection[:,7] = -1
            new_detection[:,8] = -1
            new_detection[:,9] = -1
            final_detections.append(new_detection)

            
    final_detections = np.vstack(final_detections)
    print(final_detections)
    print(final_detections.shape)
    print(final_detections[final_detections[:,0] == 0])
    
    fmt = '%d,%d,%.3f,%.3f,%.3f,%.3f,%.3f,%d,%d,%d'
    np.savetxt('lidar-2019-02-12-13-13-24.txt',final_detections, fmt=fmt)
if __name__ == "__main__":
    main()
