# go through the dataset, find number of anchor boxes and their height and width
import os
import os.path
import sys
import numpy as np
import matplotlib.pyplot as plt
import pylab
from sklearn.cluster import KMeans
import matplotlib.patches as patches

train_label_directory = 'data/camel/labels/train'
val_label_directory = 'data/camel/labels/val'
plot_data = False
visualize_anchors = False
num_clusters = 3
bbox_dimensions = []
widths = []
heights = []
image_width = 128
image_height = 32

# crawl through labels and gather all normalized height and width
def get_normalized_bbox_dimensions(directory):
    for path, directories, files in os.walk(directory):
        for file in files:
            for line in open(os.path.join(path, file)):
                category, x, y, w, h = line.split()
                bbox_dimensions.append({w, h})
                widths.append(w)
                heights.append(h)

# k-means clustering of data
def get_cluster_centroids(X, num_clusters = 3):
    # Number of clusters
    kmeans = KMeans(n_clusters=num_clusters)
    # Fitting the input data
    kmeans = kmeans.fit(X)
    # Getting the cluster labels
    labels = kmeans.predict(X)
    # Centroid values
    centroids = kmeans.cluster_centers_
    inertia = kmeans.inertia_

    return labels, centroids, inertia

def show_data_scatter(X, clusters, centroids):
    colors = ['r', 'g', 'b', 'y', 'c', 'm']
    #fig, ax = plt.subplots()
    #fig.add_axes([0,0,1,1])
    plt.figure()
    k = centroids.shape[0]
    for i in range(k):
        points = np.array([X[j] for j in range(len(X)) if clusters[j] == i])
        plt.scatter(points[:, 0], points[:, 1], s=7, c=colors[i])
    plt.scatter(centroids[:, 0], centroids[:, 1], marker='*', s=100, c='#050505')
    plt.show()

def calculate_iou(X, clusters, centroids):
    num_clusters = centroids.shape[0]
    max_ious = np.zeros((num_clusters))
    num_cluster_points = np.zeros((num_clusters))
    num_points = X.shape[0]
    # for every cluster, calculate max iou
    for i in range(num_points):
        cluster = clusters[i]
        centroid = centroids[cluster]
        num_cluster_points[cluster] += 1
        # calculate ioum
        bbox_area = float(X[i,0]) * float(X[i,1])
        anchor_area = centroid[0] * centroid[1]
        intersection_area = np.minimum(float(X[i,0]), centroid[0]) * np.minimum(float(X[i,1]), centroid[1])
        union_area = bbox_area + anchor_area - intersection_area
        #print(bbox_area, anchor_area)
        #iou = np.minimum(bbox_area, anchor_area)/(bbox_area + anchor_area)
        iou = intersection_area/union_area
        max_ious[cluster] += iou
        '''
        if iou > max_ious[cluster]:
            max_ious[cluster] = iou
        '''

    max_ious = max_ious/num_cluster_points
    return max_ious

def visualize_anchor_boxes(centroids):
    # Create figure and axes
    fig,ax = plt.subplots(1)

    for centroid in centroids:
        # Create a Rectangle patch
        width = centroid[0]*image_width
        height = centroid[1]*image_height
        x_lower_left = image_width/2 - width/2
        y_lower_left = image_height/2 - height/2
        print(x_lower_left, y_lower_left, width, height)
        plt.imshow(np.zeros((image_height, image_width)))
        rect = patches.Rectangle((x_lower_left, y_lower_left),width, height,linewidth=1,edgecolor='r',facecolor='none')
        ax.add_patch(rect)

    plt.show()

get_normalized_bbox_dimensions(train_label_directory)
#get_normalized_bbox_dimensions(val_label_directory)
#print(bbox_dimensions)
'''
widths = []
heights = []
for w,h in bbox_dimensions:
    widths.append(w)
    heights.append(h)
'''
X = np.array(list(zip(widths, heights)))
#print(X.shape)

num_anchors = range(2,10)
iou_array = np.zeros((len(num_anchors), 2))
kmeans_loss = np.zeros((len(num_anchors), 2))
count=0

for num_clusters in num_anchors:
    clusters, centroids, inertia = get_cluster_centroids(X, num_clusters)
    kmeans_loss[count] = np.array(([num_clusters, inertia]))
    #print(centroids)

    # scatter plot h,w
    if plot_data:
        show_data_scatter(X, clusters, centroids)

    if visualize_anchors:
        visualize_anchor_boxes(centroids)

    max_ious = calculate_iou(X, clusters, centroids)
    #print(max_ious)
    mean_max_iou = np.mean(max_ious)
    iou_array[count] = np.array(([num_clusters, mean_max_iou]))
    count+=1

plt.plot(iou_array[:,0], iou_array[:,1]) 
plt.xlabel('number of anchor boxes') 
plt.ylabel('mean iou') 
plt.show() 

plt.figure()
plt.plot(kmeans_loss[:,0], kmeans_loss[:,1])
plt.xlabel('number of clusters') 
plt.ylabel('k-means loss') 
plt.show()

