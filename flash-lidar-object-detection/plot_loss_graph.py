from models import *
from utils.utils import *
import matplotlib.pyplot as plt
import numpy as np

result_filename = 'results.txt'

# read result file and look for loss
def read_losses(line):
    results = line.split()
    xy_loss = float(results[2])
    wh_loss = float(results[3])
    conf_loss = float(results[4])
    cls_loss = float(results[5])
    total_loss = float(results[6])

    test_xy_loss = float(results[9])
    test_wh_loss = float(results[10])
    test_conf_loss = float(results[11])
    test_cls_loss = float(results[12])
    test_total_loss = float(results[13])

    precision = float(results[14])
    recall = float(results[15])
    mAP = float(results[16])

    #print(xy_loss, wh_loss, conf_loss, cls_loss, total_loss, precision, recall, mAP)

    return xy_loss, wh_loss, conf_loss, cls_loss, total_loss, test_xy_loss, test_wh_loss, test_conf_loss, test_cls_loss, test_total_loss, precision, recall, mAP

def draw_plot(x_axis, y_axis, graph_label, plot_title, x_label = "Epochs"):
    plt.figure()
    num_plots=len(y_axis)

    for i in range(num_plots):
            plt.plot(x_axis, y_axis[i], label = graph_label[i])
        
    plt.title(plot_title)
    plt.xlabel(x_label)

    # show a legend on the plot 
    plt.legend() 



epochs = []
xy_losses = []
wh_losses = []
conf_losses = []
cls_losses = []
total_losses = []

test_xy_losses = []
test_wh_losses = []
test_conf_losses = []
test_cls_losses = []
test_total_losses = []

precisions = []
recalls = []
mAPs = []

file = open(result_filename, "r")
num_epochs = 0
for line in file:
    xy_loss, wh_loss, conf_loss, cls_loss, total_loss, test_xy_loss, test_wh_loss, test_conf_loss, test_cls_loss, test_total_loss, precision, recall, mAP = read_losses(line)

    xy_losses.append(xy_loss)
    wh_losses.append(wh_loss)
    conf_losses.append(conf_loss)
    cls_losses.append(cls_loss)
    total_losses.append(total_loss)

    test_xy_losses.append(test_xy_loss)
    test_wh_losses.append(test_wh_loss)
    test_conf_losses.append(test_conf_loss)
    test_cls_losses.append(test_cls_loss)
    test_total_losses.append(test_total_loss)

    precisions.append(precision)
    recalls.append(recall)
    mAPs.append(mAP)

    # Each line represents results from one epoch
    num_epochs = num_epochs + 1
    
x_axis = np.arange(num_epochs)

draw_plot(x_axis, [xy_losses, wh_losses, conf_losses, cls_losses], ["xy loss", "wh loss", "conf loss", "cls loss"], "Training Losses")
draw_plot(x_axis, [test_xy_losses, test_wh_losses, test_conf_losses, test_cls_losses], ["xy loss", "wh loss", "conf loss", "cls loss"], "Testing Losses")
draw_plot(x_axis, [precisions, recalls, mAPs], ['precision', 'recall', 'mAP'], "Testing Performance")
draw_plot(x_axis, [total_losses, test_total_losses], ["training loss", "validation loss"], "Total Loss comparision")

plt.show() 
