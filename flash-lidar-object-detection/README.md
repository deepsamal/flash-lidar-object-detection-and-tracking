Repo branched out from: https://github.com/ultralytics/yolov3  

# Introduction

This directory contains python code for object detection on lidar data.

# Description

The repo contains inference and training code for YOLOv3 in PyTorch. The code works on Linux, MacOS and Windows. Training is done on the CAMEL dataset. **Credit to ultralytics for the original yolov3 pytorch code:** https://github.com/ultralytics/yolov3 **Credit to Joseph Redmon for YOLO:** https://pjreddie.com/darknet/yolo/.

# Requirements

Python 3.7 or later with the following `pip3 install -U -r requirements.txt` packages:

- `numpy`
- `torch >= 1.0.0`
- `opencv-python`
- `tqdm`

alternatively use yolov3.yml anaconda environment file.

# Data

Please download requisite data from- https://www.dropbox.com/s/jsmn5v64bh7labj/data.tar.gz?dl=0

After extraction place the folder 'data' in 'flash-lidar-object-detection' directory

# Training

**Start Training:** Run `train.py` to begin training after downloading CAMEL data.

**Resume Training:** Run `train.py --resume` resumes training from the latest checkpoint `weights/latest.pt`.

# Inference

Run `detect.py` to apply trained weights to images in `data/samples` folder:

**YOLOv3-tiny-lidar:** `python3 detect.py --cfg cfg/yolov3-tiny-lidar.cfg --weights weights/best.weights`

